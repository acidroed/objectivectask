//
//  TipTyperUITests.m
//  TipTyperUITests
//
//  Created by Oleksiy Deordytsya on 3/3/17.
//  Copyright © 2017 Bruno Philipe. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface TipTyperUITests : XCTestCase

@end

@implementation TipTyperUITests

XCUIApplication *app;
BOOL fileExists;
NSString *pathToFile;

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    
    app = [[XCUIApplication alloc] init];

    [app launch];
    
}

- (void)testExample {
    
    NSString *saveMenu = @"Save\u2026";
    NSString *fileMenu = @"File";
    NSString *randomText = [self randomStringWithLength:(50)];
    NSString *randomFileName = [self randomStringWithLength:(8)];
    //the path should be changed due to the particular user
    NSString *pathToFolder = @"/Users/odeord/Documents/";
    pathToFile = [NSString stringWithFormat:@"%@%@%@", pathToFolder, randomFileName, @".txt"];

    

    XCUIElement *untitledWindow = app.windows[@"Untitled"];
    [[untitledWindow.scrollViews childrenMatchingType:XCUIElementTypeTextView].element typeText:randomText];
    
    XCUIElementQuery *menuBarsQuery = app.menuBars;
    [menuBarsQuery.menuBarItems[fileMenu] click];
        [menuBarsQuery.menuItems[saveMenu] click];
    
    XCUIElement *saveSheet = untitledWindow.sheets[@"save"];
    [[[saveSheet childrenMatchingType:XCUIElementTypeTextField] elementBoundByIndex:0] typeText:randomFileName];
    [saveSheet.buttons[@"Save"] click];
    
   
    fileExists = [[NSFileManager defaultManager] fileExistsAtPath:pathToFile];
    
    
    XCTAssertTrue(fileExists, @"The file wasn't saved");
   
    
}

//method to make random String
-(NSString *) randomStringWithLength:(NSInteger)length {
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
    
    for (int i = 0; i < length; i++) {
        [randomString appendFormat:@"%C", [letters characterAtIndex:arc4random() % [letters length]]];
    }
    
    return randomString;
}

- (void)tearDown {
    if (fileExists) {
        NSError *error = [NSError new];
        [[NSFileManager defaultManager] removeItemAtPath:pathToFile error:&error];
    }
    [super tearDown];
}

@end
